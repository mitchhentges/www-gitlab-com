describe Gitlab::Homepage::Team::Member do
  describe 'team member template integration' do
    let(:team) { Gitlab::Homepage::Team.build! }

    describe 'first team member validations' do
      let(:member) { team.members.first }

      it 'correctly integrates with data/team.yml' do
        expect(member.username).to eq 'dzaporozhets'
        expect(member.country).to eq 'Ukraine'
        expect(member.story).to match /Dmitriy started GitLab/
        expect(member.roles).to be_a Hash
      end
    end

    describe 'dangling assignments validation' do
      let(:members) { team.members }
      let(:projects) { team.projects }

      it 'does not assign non-existent projects to anyone' do
        members.each do |member|
          member.roles.each_key do |key|
            expect(key).to satisfy('be a defined project key') do |k|
              projects.any? { |project| k == project.key }
            end
          end
        end
      end
    end
  end

  describe '#roles' do
    context 'when user has only one role in the project' do
      subject do
        described_class.new('projects' =>
                              { 'gitlab-ce' =>
                                'maintainer backend' })
      end

      it 'returns an inverted project role hash' do
        expect(subject.roles['gitlab-ce']).not_to be_empty
        expect(subject.roles['gitlab-ce']).to all(be_a String)
      end
    end

    context 'when user has only one role in the project' do
      subject do
        described_class.new('projects' =>
                              { 'gitlab-ce' =>
                                ['maintainer backend', 'owner'] })
      end

      it 'returns an inverted project role hash' do
        expect(subject.roles['gitlab-ce']).not_to be_nil
        expect(subject.roles['gitlab-ce']).to all(be_a String)
      end
    end
  end

  describe '#involved?' do
    subject do
      described_class.new('projects' =>
                            { 'gitlab-ce' =>
                              ['maintainer backend', 'owner'] })
    end

    context 'when user is involved in the project' do
      it 'indicates that user is involved in the project' do
        project = double(key: 'gitlab-ce')

        expect(subject.involved?(project)).to be true
      end
    end

    context 'when user is not involved in the project' do
      it 'indicates that user is not involved in the project' do
        project = double(key: 'gitlab-ee')

        expect(subject.involved?(project)).to be false
      end
    end
  end
end
