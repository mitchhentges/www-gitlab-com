---
layout: markdown_page
title: "Infrastructure"
---

## Common Links

- [Public Infrastructure Issue Tracker](https://gitlab.com/gitlab-com/infrastructure/issues/); please use confidential issues for topics that should only be visible to team members at GitLab. No longer active, but kept for reference, is the legacy public [Operations issue tracker](https://gitlab.com/gitlab-com/operations/issues) as well.
- [Chat channel](https://gitlab.slack.com/archives/infrastructure); please use the `#infrastructure` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.
- [GitLab Production Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com&ctz=America/Los_Angeles); add it to your own views by hitting the `+GoogleCalendar` button in the lower right of the screen when viewing the Calendar with the link here.

## On this page
{:.no_toc}

- TOC
{:toc}

## Related Pages

- [Security Team Handbook](security/)
- [On Call](/handbook/on-call/).
- [GitLab Cloud Images](https://about.gitlab.com/cloud-images/).

## Infrastructure teams

The infrastructure team is populated by engineers who share the responsibility of making GitLab.com scale, keep it safe, available and scalable with specific focuses.

These teams are:

* Production: keeping GitLab.com available and scalable.
* [Security](/handbook/infrastructure/security/): keeping GitLab.com safe, from the perspective of the application, the infrastructure and the organization.
* Database: keeping GitLab.com's database fast and scalable.
* Gitaly: making Git access scalable and fast.

### Production Team

Composed of production engineers.

Production engineers work on keeping the infrastructure that runs our services
running fast and reliably.  This infrastructure includes staging, GitLab.com and
dev.GitLab.org.

Production engineers also have a strong focus on building the right toolsets
and automations to enable development to ship features as fast and bug free as
possible, leveraging the tools provided by GitLab.com itself - we must dogfood.

Another part of the job is building monitoring tools that allow quick
troubleshooting as a first step, then turning this into alerts to notify based on
symptoms, to then fixing the problem or automating the remediation. We can only scale
GitLab.com by being smart and using resources effectively, starting with our
own time as the main scarce resource.

[Production Engineer](jobs/production-engineer/index.html) job description.

#### Tenets

1. Security: reduce risk to its minimum, and make the minimum explicit.
1. Transparency, clarity and directness: public and explicit by default, we work in the open, we strive to get signal over noise.
1. Efficiency: smart resource usage, we should not fix scalability problems by throwing more resources at it but by understanding where the waste is happening and then working to make it disappear. We should work hard to reduce toil to a minimum by automating all the boring work out of our way.

#### Production and Staging Access

Production access is granted to production engineers, security engineers, and (production) on-call heroes.

Staging access is treated at the same level as production access because it contains production data.

Any other engineer, or lead, or manager at any level will not have access to production, and, in case some information is needed from production it must be obtained by a production engineer through an issue in the infrastructure issue tracker.

There is one temporary exception: [release managers](/release-managers) require production access to perform deploys, they will have production access until production engineering can offer a deployment automation that does not require chef nor ssh access. This is an ongoing effort.

#### Production Engineering Resources

- Documentation: refer to runbooks and internal documentation on this very page.
- On-call Log: [document](https://docs.google.com/document/d/1nWDqjzBwzYecn9Dcl4hy1s4MLng_uMq-8yGRMxtgK6M/edit#heading=h.nmt24c52ggf5) where we write events that happen in production - internal use only - editable by the whole company
- Chat channels in Slack:
  - Prometheus-alerts: monitoring tools post into this channel, production engineers should monitor this channel to act on alerts. Acting may be remediating or just fixing noisy alerts.
  - Infrastructure: general conversation about infrastructure goes on in this channel. Remember to let the people know when you are about to do some change in the infrastructure.
- Operations channel archive:
  - You can find the infrastructure archive [here](https://docs.google.com/document/d/19yzyIHY9F_m5p0B0e6STSZyhzfo-vLIRVQ1zRRevWRM/edit#heading=h.lz1c6r6c9ejd).
-  Automated tasks and schedules
  - Weekly automatic OS updates are performed on Monday at 10:10 UTC.
- Monitoring: we do monitoring with prometheus leveraging available exporters like the node or the postgresql exporters, and we build whatever else is necessary within production engineering itself. We maintain 2 monitoring infrastructures:
  - [Public monitoring infrastructure](http://monitor.gitlab.net/):
    - No auth is required
    - Is automatically sync from the private monitoring infrastructure on every chef client execution. Don't change dashboards here, they will be overwritten.
  - [Private monitoring infrastructure](https://performance.gitlab.net):
    - Highly Available setup
    - Alerting feeds from this setup
    - Private GitLab account is required to access
    - Separated from the public for security and availability reasons, they should have exactly the same graphs after we deprecate InfluxDB

## Documentation

### Runbooks

[Runbooks are public](https://gitlab.com/gitlab-com/runbooks), but they are
automatically mirrored in our [development environment](https://dev.gitlab.org/cookbooks/runbooks/),
this is so because if GitLab.com is down, those runbooks would not be available
to take it up again.

These runbooks aim to provide simple solutions for common problems, they are
linked pointed from our alerting system and should also be kept up to date with
whatever new finding we get as we learn how to scale GitLab.com so these
runbooks can also be adopted by our customers.

Runbooks are divided into 2 main sections:

- What to do when: points to specific runbooks to run on stressful situations (on-call)
- How do I: points to general administration texts that explain how to perform different administration tasks.

When writing a new runbook, be mindful what the goal of it is:

- If it is for on-call situations, make it crisp and brief. Try to keep the following structure: pre-check, resolution, post-check .
- If it is for general management, it can be freely formatted.

### Chef cookbooks

Some basic rules:

- Use maintained cookbooks from https://supermarket.chef.io.
- Create a wrapper cookbook whenever a feature is missing.
- Make sure our custom cookbooks are public available from https://gitlab.com/gitlab-cookbooks.
- Make sure there is a copy in our DEV environment https://dev.gitlab.org/cookbooks and setup push mirror to keep it in sync.
- Berkshelf should only point to our cookbooks in DEV so we are able to fix our cookbooks whenever GitLab.com comes unavailable.
- Cookbooks should be developed using the team. We use merge requests and code review to share knowledge and build the best product we can.
- Cookbooks should be covered with ChefSpec and TestKitchen testing in order to ensure they do what they are supposed to and don't have conflicts.

Generally our [chef cookbooks](https://gitlab.com/groups/gitlab-cookbooks) live in the open, and they get mirrored back to our
[internal cookbooks group](https://dev.gitlab.org/cookbooks) for availability reasons.

There may be cases of cookbooks that could become a security concern, in which case it is ok to keep them in our GitLab
private instance. This should be assessed in a case by case and documented properly.

### Internal documentation

Available in the [Chef Repo](https://dev.gitlab.org/cookbooks/chef-repo).
There is some documentation that is specific to GitLab.com. Things that are specific to our infrastructure
providers or that would create a security threat for our installation.

Still, this documentation is [in the Chef Repo](https://dev.gitlab.org/cookbooks/chef-repo), and we aim to
start pulling things out of there into the runbooks, until this documentation is thin and GitLab.com only.

## Production events logging

There are 2 kind of production events that we track:

- Changes into the production fleet: for this we record things [in the Chef Repo](https://dev.gitlab.org/cookbooks/chef-repo).
  - Deploys will be recorded automagically because of the way we do deploys.
  - General operations can be recorded by creating an empty commit in the repo and pushing it into origin.
- Outages and general production incidents
  - If we are required to act in production manually to perform any operation we should create an issue and consider labeling it as _toil_ to track the cost of such manual work load.
  - It we had a disruption in the service, we must create a blameless post mortem. Refer to the _Outages and Blameless Post Mortems_ section ahead

## Outages and Blameless Post Mortems

Every time there is a production incident we will create an issue in the [infrastructure
issue tracker](https://gitlab.com/gitlab-com/infrastructure/issues) with the _outage_ label.

In this issue we will gather the following information:
* The timeline of events: what happened first, what later, what reasoning triggered what action.
* Sample graphs or logs captured from our monitoring explaining how they drove our reasoning.
* [The 5 whys](https://en.wikipedia.org/wiki/5_Whys) that lead to the root cause that triggered the incident.
* The things that worked well
* The things that can be improved
* Further actions with links to the issues that cover them

These issues should also be tagged with any other label that makes sense, for
example, if the issue is related to storage, label it so.

The responsibility of creating this post mortem is initially on the person that
handled the incident, unless it gets assigned explicitly to someone else.

### Public by default policy

These blameless post mortems have to be public by default with just a few exceptions:
* The post mortem would affect a customer or employee privacy: revealing the real user name, email, private project names, any data that can identify the person, etc.
* The post mortem would reveal billing information.
* The post mortem would reveal GitLab's confidential information.

That's it, there are no more reasons.

If what's blocking us from revealing this information is shame because we made a mistake, that is not a good enough reason.

The post mortem is blameless because our mistakes are not a person mistake but a company mistake, if we made a bad decision because our monitoring failed we have to fix our monitoring, not blame someone for making a decision based on insufficient data.

On top of this, blameless post-mortems help in the following aspects:

* We can help people understand the complexity of running a service in production, and how things can go wrong.
* We help ourselves to learn by reflecting and analyzing on why this issue has happened.
* We force ourselves to think about what we need to do to not make the same mistake again, or to improve our infrastructure in a way that we don't have to deal with the same incident.
* We open our reasoning and information to the public so they can chime in and help us out.
* We leave a great trace of information for onboarding new engineers. They can see how production fails.
* We can use these post-mortems for recruiting and marketing.

Once this Post Mortem is created, we will tweet from the GitLabStatus account with a link to the issue and a brief explanation of what is it about.


## Making Changes to GitLab.com

The production environment of GitLab.com should be treated with care since we
strive to make GitLab.com so reliable that it can be mission-critical for our
customers. _Allowable downtime_  is a scarce resource.

Therefore, to be able to deploy useful and cool new things into production,
we need to
- use checklists to prepare and carry out the changes, and
- schedule the (non-automated, non-self-serve) changes we make to our
production environment (to be sure the necessary people are there, and to prevent having multiple changes happening at the same time).

### Changes that need a checklist and a schedule

For example:
- When you introduce something _new_ such as installing `pgbouncer`, enabling ElasticSearch, etc.
- Anything that _may_ result in downtime. Not sure? Then assume that it will result in downtime.
- Anything that constitutes a change to GitLab.com _and_ requires the assistance of a Production Engineer.

### Changes that _may_ need a checklist, but not explicit scheduling

For example:
- A self-service process, such as adding alerting (which only requires a
  merge request to the relevant repo), or fixing a chef-client.
- "Quick" fixes that do _not_ require a Production Engineer.

### Changes in staging

Testing things in staging typically only needs scheduling to avoid conflicting with others, but is otherwise straightforward since it is mostly self-service.

- Consider using the same format of the checklist - it is good form to know
how to roll back for example. Using the checklist will also set the baseline for
when you want to introduce the change into production; thus saving time. But it
is not required to follow the full checklist in order to make changes on staging.
- **Do** schedule changes to staging, on the "GitLab Production" calendar (link at [top of page](#common-links)). This
is to prevent conflicts when different people or teams are trying to work on
staging for different purposes.
- Given that there are quite a few people who can use self-service in staging,
there should be no need for resourcing there from the Production team, and you
do not need an explicit OK from the Production Lead.

### Deployments

Deployments of release(s) (candidates) are a special case. We don't want to block
deployments if we can avoid it. And since they are currently performed by release managers there is
generally no need for someone from production engineering to be heavily involved.
Still, follow the next steps to schedule a deploy:

* **Schedule the deploy** both in staging and production on the "GitLab Production"
calendar (link at [top of page](#common-links)) as soon as possible. Do this to avoid conflicting with any other
production operation that will happen (don't oversubscribe).
* Use a 4 hr block too, sometimes deploys delay to start for odd reasons, you
don't want to run out of time.
* Notify the production engineering team by inviting ops-contact. This will raise
awareness.
* Notify via twitter the range of time when the deploy will happen.
* If you need help from production engineering for whatever reason, be explicit
in the invite, or get in touch in the production chat channel to ask.

### Production Change Checklist

Any team or individual can initiate a change to GitLab.com by following this checklist. Create an issue in the infrastructure [issue
 tracker](https://gitlab.com/gitlab-com/infrastructure/issues) and provide the
 following (copy/paste this list):

```
### PLANNING THE CHANGE

- [ ] Context: _What is the background of the change? Relevant links?_
- [ ] Downtime: _Will the change introduce downtime, and if so, how much?_
  - [ ] What options were considered to avoid downtime?
  - [ ] What is the downtime estimate based on? Can it be tested in some way?
- [ ] People:
   - [ ] Who will be present? Who will handle communications (twitter, banner, google doc, etc.)? What other roles will be needed?
- [ ] Pre-checks: _What should we check before starting with the change? Consider dashboards, metrics, limits of current infrastructure, etc._
  - [ ] Does the change alter how we use Azure or how many of Azure's resources we use? If
  so, consider opening an "advisory ticket" in the Azure portal to get input from their team.
- [ ] Change Procedure:
  - [ ] List the steps that are needed for the change; be as granular as possible.
  - [ ] Did you do a dry run to test / measure performance and timings? 
- [ ] Preparatory Steps: _What can be done ahead of time? How far ahead?_
   - When determined what these may be, add them to the "Preparatory Steps" section below under "Doing the Change".
- [ ] Post-checks: _What should we check after the change has been applied?_
  - [ ] How do we know the change worked well? How do we know the change did not work well? What monitoring do we need to pay attention to? How do we verify data integrity?
  - [ ] Should any alerts be modified as a consequence of this change?
- [ ] Rollback procedure: _In case things go wrong, what do we need to do to recover?
Also consider rolling back from an intermediate step: does the procedure change
depending on how far along the procedure is?_
- [ ] Create an invite using a 4 hr block of time on the "GitLab Production"
calendar (link in [handbook](https://about.gitlab.com/handbook/infrastructure/#common-links)), inviting the ops-contact group.
 Include a link to the issue. (Many times you will not expect to need - or actually
  need - all 4 hrs, but past experience has shown that delays and unexpected events
  are more likely than having things go faster than expected.)
- [ ] Ping the Production Lead in this issue to coordinate who should be present
from the Production team, and to confirm scheduling.
- [ ] When will this occur? _leave blank until scheduled_
- [ ] Communication plan:
   - [ ] Tweet: default to tweeting when schedule is known, then again 12 hrs before, 1 hr
   before, when starting, during if there are delays, and after when complete.
   - [ ] Deploy banner: display warning 1 hr before
   - [ ] Other?


### DOING THE CHANGE

#### Preparatory steps
- [ ] Copy/paste items here from the Preparatory Steps listed above.

#### Initial Tasks
- [ ] Create a google doc to track the progress. This is because in the event of an
outage, Google docs allow for real-time collaboration, and don't depend on
GitLab.com being available.
  - [ ] Add a link to the issue where it comes from, copy and paste the content of the
  issue, the description, and the steps to follow.
  - [ ] Title the steps as "timeline". Use UTC time without daylight saving, we all are in
  the same timezone in UTC.
  - [ ] Link the document in the [on-call log](https://docs.google.com/document/d/1nWDqjzBwzYecn9Dcl4hy1s4MLng_uMq-8yGRMxtgK6M/edit#) so it's easy to find later.
  - [ ] Right before starting the change, paste the link to the google doc in the #production chat channel and "pin" it.
- [ ] Discuss with the person who is introducing the change, and go through the plan to fill the gaps of understanding before starting.
- [ ] Final check of the rollback plan and communication plan.
- [ ] Set PagerDuty maintenance window before starting the change.

#### The Change
- [ ] Start running the changes. When this happens, one person is making the change, the
other person is taking notes of when the different steps are happening. Make it explicit
who will do what.
- [ ] When the change is done and finished, either successfully or not, copy the content
of the document back into the issue and deprecate the doc (and close the issue if possible).
- [ ] Retrospective: answer the following three questions:
  - [ ] What went well?
  - [ ] What should be improved?
  - [ ] Specific action items / recommendations.
- If the issue caused an outage, or service degradation, label the issue as "outage".
```

## Make GitLab.com settings the default

As said in the [production engineer job description](jobs/production-engineer/index.html)
one of the goals is "Making GitLab easier to maintain to administrators all over the world".
One of the ways we do it is making GitLab.com settings the default for all our customers.
It is very important that GitLab.com is running GitLab Enterprise Edition with all its default settings.
We don't want users running GitLab at scale to run into any problems.

If it is not possible to use the default settings the difference should be documented in
[GitLab.com settings](https://about.gitlab.com/gitlab-com/settings/#gitlab-pages)
_before_ applying them to GitLab.com.

## Involving Azure

- GitLab has access to "Azure Rapid Response". The level of support is described
on [their website](https://azure.microsoft.com/en-us/support/plans/response/),
but TL;DR it means that the SLAs are:
   - Initial response for Sev A < 15 mins, Sev B < 2 Hours, Sev C < 4 hours.
- The Azure team prefers if we ask questions via the support portal, but there is
an Azure representative whom we can ping in our issue tracker. This is mostly
helpful in cases of non-urgent questions, and questions of an advisory nature.
- Light hand-holding for things such as architecture review is available. To
reach out for this assistance, submit an "advisory case" ticket through the portal.

* Azure subscription and service limits, quotas, and constraints: https://docs.microsoft.com/en-us/azure/azure-subscription-service-limits
